# Alzheimer's Disease Data Analysis


# Description
Brain tissue has long been studied and detailed. Currently there exist both open source and proprietary tools to convert 2 dimensional images from stacks or voxels into 3 dimensional reconstructions. In cases where image qualities are low, or the structure of the brain has altered, images must be pre-processed before rendering can take place.
We have created stand-alone models based trained on mouse brain samples that, when combined with our image processing pipeline, model retraining, and 3 dimensional rendering, improve image segmentation and component prediction by nearly 30%. In addition our rendered images are manipulate-able in real time, with component selection, zoom pan and tilt manipulation as well as luminosity and opacity controls. Additionally, to ensure the pre-trained models can be optimized for any environment, we have created a custom tool allowing for humans to interrupt the pipeline, retrain the model in real time, and re-deployed on any image database.
Our pipeline is inherently scalable and is written to be deployed in a remote computer cluster environment allowing for thousands of pixels to be evaluated, modeled and modified without artificial thresholds caused by individual computing resources.
Our pipeline has been tested on multiple datasets of differing qualities and initial deployment on human tissue samples has been started by our advising team.


# Team Members
- Matthew L. Bond <m1bond@ucsd.edu>
- Doreh Kazemisefat <dkazemis@ucsd.edu>
- Ashok Mondal <asmondal@ucsd.edu>
- Peishan Sha <psha@ucsd.edu>
- Daniel Silva <dasilva@ucsd.edu>


# Advisors
- Dr. Mark Ellisman <mellisman@ucsd.edu>
- Matthew Madany <madany@ucsd.edu>
- Steve Peltier <peltier@ncmir.ucsd.edu>


# Jupyter Notebooks
https://gitlab.com/dan.silva/dse260-capstone

| notebook                             | Description                                                                                                                                                                                           |
|--------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| cyclegan_train_test.ipynb            | This is an all-in-one notebook that will generate all training and testing data. Afterwards it will execute both the train and test scripts.                                                          |
| cyclegan_train_test-Multiscale.ipynb | This is an all-in-one notebook that will generate all training and testing data. It will generate data that has multiple scaling factors. Afterwards it will execute both the train and test scripts. |
| gan_translate.ipynb                  | Notebook used for tiling images prior to training a CycleGAN model. Notebook is also used to stitch together the images from the CycleGAN output.                                                     |
| EM_Analysis_Cortext1-Grp3.ipynb      | This notebook will run the image segmentation models for both cell membranes and mitochondria. It will then run the 3D volume rendering based on the predicted output.                                |
| ModelPerformance-Organelle.ipynb     | Will generate tables and visualizations for analysis for outputs of the several trained CDeep3M models.                                                                                               |



# Manual Segmentation Tool
https://gitlab.com/dan.silva/manseg

This is a lightweight manual segmentation tool used for generating labeled data for retraining a CDeep3M model. Instructions can be found in its corresponding repository.


# Pre-trained Models
## CycleGAN
This is a pre-trained CycleGAN model that has been trained on 200 images from the cortex dataset. 
The data used for training was scaled down to 50%, 60%, and 70% of the original size.  
 It was trained with the following hyperparameters:


|Hyperparameter| Value  |
|--------------|--------|
|epoch         |1600    |
|learning rate |0.000002|

To run the CycleGAN model refer to the following repository for setting up the environment:
https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix
```
git clone https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix.git cyclegan
cd cyclegan
mkdir checkpoints
```

Unzip the zipped model into the `checkpoints` directory, which should yield the following directory: `cortex_1_scaled_567_1600e_000002`

```
unzip cyclegan_model.zip
```
Example command:
```
python test.py --dataroot <path-to-sample-data>
            --name cortex_1_scaled_567_1600e_000002 \
            --model test \
            --gpu_ids 0 \
            --batch_size 1 \
            --no_dropout \
            --dataset_mode unaligned \
            --preprocess none
```
## CDeep3M Models
There are two pre-trained CDeep3M models provided:
- membrane model - re-trained to 50800 (50000+800) iterations
- mitochondria model - re-trained to 40800 (40000+800) iterations  

Both models have been trained an additional 800 iterations from the original model. They have been trained with 10 images pertaining to the cortex dataset.
```
unzip cdeep3m_models.zip
```


To run the pre-trained models, refer to the following repository for setting up the environment:
https://github.com/CRBS/cdeep3m2
```
git clone https://github.com/CRBS/cdeep3m2.git cdeep3m
cd cdeep3m
```
Example command:
```
runprediction.sh --model 3fm --denoise 0 --augspeed 4 membrane_model_800_iter sample_data predicted_membranes
```

# Training CDeep3M
Example commands:
```
# preprocess training data
PreprocessTrainingData.m sample-data/images/ sample-data/labels/ sample-data/augmented

# run training as a background process
runtraining.sh --additerations 800 --retrain  sample-data/augmented/ SEMTEM_membranes_TrainedNet &
```